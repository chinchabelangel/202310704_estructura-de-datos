<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MemoriaController extends Controller
{
    public function Memoria(){
        $Resultado = "";
        $Resultado.="Memoria en uso: " . memory_get_usage(). "( ". ((memory_get_usage() / 1024) / 1024) . "M) <br>";
        $Resultado.=str_repeat("a",2000)."<br>";
        $Resultado.="Memoria en uso: " . memory_get_usage(). "( ". ((memory_get_usage() / 1024) / 1024) . "M) <br>";
        $Resultado.="Memoria limite: " . ini_get("memory_limit") . "<br>";
        return view("memoria",["Resultado"=>$Resultado]);
    }
}
